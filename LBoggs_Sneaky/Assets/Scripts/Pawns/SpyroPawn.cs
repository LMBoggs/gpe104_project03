﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpyroPawn : Pawn {

    //Declare variables
    public float walkSpeed; // designer friendly var to set walk speed
    public float runSpeed; // designer friendly var to set run speed
    public float speed; // var to calculate which speed value to use. Public for noisemaker code, NOT FOR DESIGNERS
    public bool isFlipped; // bool to store if the pawn is currently flipped in the X scale of transform. Designer friendly
    public NoiseMaker myNoise; // var to hold noisemaker on this object

    //private Transform tf; // var to hold pawn transform
    private Animator anim; // var to hold pawn animator
    private Vector3 mirrorVector; // var to hold vector to mirror pawn over x transform when multiplied to current transform position

    // Use this for initialization
    public override void Start ()
    {
        //Use parent start function
        base.Start();

        //make sure noisemaker is attached
        myNoise = GetComponent<NoiseMaker>();

        //load animator to variable
        anim = GetComponent<Animator>(); 
    }

    // Update is called once per frame
    public override void Update ()
    {
        //Call Parent update
        base.Update();

        //assign Mirror Vector on every frame
        mirrorVector = new Vector3(tf.localScale.x * -1, tf.localScale.y, tf.localScale.z); // flips the x in pos and neg, y and z remain the same

        //*******************************************************
        // SPEED AND VOLUME SETTER //Set Walking or Running, or stopped
        //********************************************************

        //If run is held with some other movement button --> RUN
        if (Input.GetButton("Run") && (Input.GetButton("Up") || Input.GetButton("Down") || Input.GetButton("Left") || Input.GetButton("Right")))
        {
            speed = runSpeed; // calculate with run speed if run button is being held
            myNoise.volume = 7;
        }

        //Else if run is NOT held, but another movement button is being held --> WALK
        else if (!Input.GetButton("Run") && (Input.GetButton("Up") || Input.GetButton("Down") || Input.GetButton("Left") || Input.GetButton("Right")))
        {
            speed = walkSpeed; // use walk speed if run button is not being held
            myNoise.volume = 4;
        }

        //else if no button is being held, then speed is STOPPED (0)
        else if (!Input.GetButton("Up") && !Input.GetButton("Down") && !Input.GetButton("Left") && !Input.GetButton("Right"))
        {
            speed = 0; //player is not moving 
        }


    }

        
    //*******************************************************
    //Movement Operations
    //*******************************************************
    public override void MoveN()
    {
        tf.position += (Vector3.up * speed);
        anim.SetInteger("pawnFacing", 1);
    }

    public override void MoveS()
    {
        tf.position += (Vector3.down * speed);
        anim.SetInteger("pawnFacing", 5);
    }

    public override void MoveE()
    {
        tf.position += (Vector3.right * speed);
        anim.SetInteger("pawnFacing", 3);

        //if pawn is flipped, then flip back and update bool
        if (isFlipped)
        {
            tf.localScale = mirrorVector;
            isFlipped = false;
        }
    }

    public override void MoveW()
    {
        tf.position += (Vector3.left * speed);
        anim.SetInteger("pawnFacing", 3);

        //if pawn is NOT flipped, then flip and update bool
        if (!isFlipped)
        {
            tf.localScale = mirrorVector;
            isFlipped = true;
        }
    }

    public override void MoveNE()
    {
        tf.position += ((Vector3.up + Vector3.right) * speed);
        anim.SetInteger("pawnFacing", 2);

        //if pawn is flipped, then flip back and update bool
        if (isFlipped)
        {
            tf.localScale = mirrorVector;
            isFlipped = false;
        }
    }

    public override void MoveSE()
    {
        tf.position += ((Vector3.right + Vector3.down) * speed);
        anim.SetInteger("pawnFacing", 4);

        //if pawn is flipped, then flip back and update bool
        if (isFlipped)
        {
            tf.localScale = mirrorVector;
            isFlipped = false;
        }
    }

    public override void MoveSW()
    {
        tf.position += ((Vector3.left + Vector3.down) * speed);
        anim.SetInteger("pawnFacing", 4);

        //if pawn is NOT flipped, then flip and update bool
        if (!isFlipped)
        {
            tf.localScale = mirrorVector;
            isFlipped = true;
        }
    }

    public override void MoveNW()
    {
        tf.position += ((Vector3.left + Vector3.up) * speed);
        anim.SetInteger("pawnFacing", 2);

        //if pawn is NOT flipped, then flip and update bool
        if (!isFlipped)
        {
            tf.localScale = mirrorVector;
            isFlipped = true;
        }
    }

    public override void TeleportStart()
    {
        //teleport to start position
        tf.transform.position = startPosition;
    }
    
    //Spyro does not have "GoHome" function



} //end of class
