﻿//FINITE STATE MACHINE FOR "DIAMOND" THE CAT

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DiamondPawn : Pawn {

    //DECLARE VARIABLES
    //-----------------------------------
    //public float aiSenseRadius; // If the player is closer than this, we will chase
    public GameObject targetObject; //var to hold player or waypoint object
    public Vector3 destination; //Diamond's destination target point
    public GameObject[] Waypoints; //array of waypoints for Diamond to cycle through during patrol state
    public int wp_index; // var to hold current waypoint index 
    public TextMeshProUGUI ReactionGUI; //store reaction textmeshpro text for Diamond


    //Tank and heat-seeking variables
    public float rotateSpeed; // designer friendly var to hold rotation speed
    public float walkSpeed; //designer friendly var to hold walk speed
    public float chaseSpeed; //var to hold chase speed, designer friendly

    private Vector3 myPosition; // var to hold my position
    private Vector3 directionVector; // var to hold vector, calculate direction to travel
    private Vector3 moveVector; // var to hold and calculate direction and speed for movement

    private Vector3 myRotation; // var to hold start rotation vector of enemy
    private Vector3 destinationRotation; // var to hold rotation of player or waypoint (destination)
    private float angle; // var to hold float, calculate angle to travel
    Quaternion targetRotation; // var to hold and calculate rotation

    //START
    //---------------------------------------------------------
    // Use this for initialization
    public override void Start () {

        //Set Variables
        base.Start();
        myPosition = tf.position; 
        wp_index = 0; // start at first waypoint in array
        destination = Waypoints[wp_index].transform.position; //set destination to the first waypoint in the array
        
       

	}

    // Update is called once per frame
    public override void Update ()
    {

    }


    //Investigate state (hears player)
    public override void Investigate()
    {
        //MOVEMENT
        //---------------------------------
        //Update variables every frame
        myPosition = tf.position; // store my position on every frame
        destination = GameManager.instance.playerController.pawn.transform.position; //store position of player on every frame

        directionVector = destination - myPosition; //store diff between destination and cat as direction on every frame
        directionVector.Normalize(); // normalize the vector to a magnitude of 1

        moveVector = directionVector * walkSpeed * 0.75f; //calculate the new vector

        //Move towards target location (player)
        tf.position += moveVector; //move in that direction each frame at given speed on every frame

        //ROTATION
        //--------------------------------
        //Update variables every frame

        angle = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg; //calculate angle based on direction to player
        targetRotation = Quaternion.Euler(0, 0, angle); //update angle to rotate towards
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * 0.75f * Time.deltaTime); //gradually rotate towards angle times speed

        //Set Reaction Text
        //------------------------------------
        ReactionGUI.SetText("?");
}




    //Caught player state
    void Caught()
    {
        //teleport ai pawn to start point
        base.TeleportStart();

        //teleport player pawn to startpoint
        GameManager.instance.playerController.pawn.TeleportStart();

        //set text to blank
        ReactionGUI.SetText("");
    }

    public override void Chase()
    {
        //MOVEMENT
        //---------------------------------
        //Update variables every frame
        myPosition = tf.position; // store my position on every frame
        destination = GameManager.instance.playerController.pawn.transform.position; //store position of player on every frame

        directionVector = destination - myPosition; //store diff between destination and cat as direction on every frame
        directionVector.Normalize(); // normalize the vector to a magnitude of 1

        moveVector = directionVector * chaseSpeed; //calculate the new vector

        //Move towards target location (player)
        tf.position += moveVector; //move in that direction each frame at given speed on every frame

        //ROTATION
        //--------------------------------
        //Update variables every frame

        angle = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg; //calculate angle based on direction to player
        targetRotation = Quaternion.Euler(0, 0, angle); //update angle to rotate towards
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime); //gradually rotate towards angle times speed

        //Set Reaction Text
        //----------------------------
        ReactionGUI.SetText("!");
    }

    public override void Patrol()
    {
        //MOVEMENT
        //---------------------------------
        //Update variables every frame
        myPosition = tf.position; // store my position on every frame
        destination = Waypoints[wp_index].transform.position; //store position of waypoint

        directionVector = destination - myPosition; //store diff between destination and cat as direction on every frame
        directionVector.Normalize(); // normalize the vector to a magnitude of 1

        moveVector = directionVector * walkSpeed; //calculate the new vector

        //Move towards target location (player)
        tf.position += moveVector; //move in that direction each frame at given speed on every frame


        //ROTATION
        //--------------------------------
        //Update variables every frame

        angle = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg; //calculate angle based on direction to player
        targetRotation = Quaternion.Euler(0, 0, angle); //update angle to rotate towards
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime); //gradually rotate towards angle times speed

        //Set Reaction Text
        //----------------------------
        ReactionGUI.SetText("");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //If collide with waypoint
        if (other.gameObject.tag == "Waypoint")
        {
            //If it is out
            if (other.gameObject.transform.position == destination)
            {
                //increment waypoint index
                wp_index++;

                //if index goes above length of array, return index to 0
                if (wp_index > Waypoints.Length || wp_index == 19)
                {
                    wp_index = 0;
                }

                //set destination to new waypoint index
                destination = Waypoints[wp_index].transform.position;
            }
        }

        if (other.gameObject.tag == "Player")
        {
            Caught(); //the player is caught
            wp_index = 0; //reset waypoint system
            GameManager.instance.playerLives--; //decrease lives by 1
        }
    }


}
