﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    //Declare variables
    public Transform tf; //var to hold my transform
    public Vector3 startPosition; // var to store location of start position
    //public GameObject pCharacter; // store prefab of player character, designer friendly, load var in editor

    // Use this for initialization
    // May be overriden by child classes
    public virtual void Start ()
    {
        //Set variables
        tf = GetComponent<Transform>(); //store position of transform on start
        
        startPosition = tf.position; //start position is where object is on start of game
    }
	
	// Update is called once per frame
    // May be overriden by children
	public virtual void Update ()
    {
		
	}

    //Tank Movement patterns
    //------------------------------------------------------------
    public virtual void MoveForward()
    {

    }

    public virtual void MoveBackward()
    {

    }

    public virtual void RotateLeft()
    {

    }

    public virtual void RotateRight()
    {
    }



    //Cardinal Movement patterns for children (ie: Spyro)
    //-----------------------------------------------------

    public virtual void MoveN()
    {

    }

    public virtual void MoveS()
    {

    }

    public virtual void MoveE()
    {

    }

    public virtual void MoveW()
    {
    }

    public virtual void MoveNE()
    {

    }

    public virtual void MoveSE()
    {

    }

    public virtual void MoveSW()
    {

    }

    public virtual void MoveNW()
    {

    }

    //Enemy AI to inherit
    //------------------------------------------
    public virtual void Patrol()
    {

    }

    public virtual void Investigate()
    {
        
    }

    public virtual void Chase()
    {

    }


    //Inclusive functions
    //--------------------------------------
    public virtual void Idle()
    {

    }

    public virtual void GoHome()
    {

    }

    public virtual void TeleportStart()
    {
        tf.position = startPosition; 
    }

}
