﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    //Declare variables
    private Pawn pawn; // variable to hold pawn of player
    private Transform tf; // variable to hold camera transform
    private Transform pawn_tf; //variable to hold transform of player's pawn

	// Use this for initialization
	void Start () {

        //Assign variables
        pawn = GameManager.instance.playerController.pawn; // load pawn controlled by the player
        tf = GetComponent<Transform>(); // load camera transform to tf var
        pawn_tf = pawn.GetComponent<Transform>(); // load pawn transform to var

        

	}
	
	// Update is called once per frame
	void Update () {

        //Transform is updated every frame to the pawn's x and y position, but maintains the camera z positing (does not rotate
        tf.position = new Vector3(pawn_tf.position.x, pawn_tf.position.y, tf.position.z);

	}


    //Call this function if the player gains control of a newly instatiated pawn other than that of game start
    void UpdateCameraPawn()
    {
        pawn = GameManager.instance.playerController.pawn; // load pawn controlled by the player
    }
}
