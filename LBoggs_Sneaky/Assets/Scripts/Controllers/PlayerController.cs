﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

 
	// Use this for initialization
	public override void Start () {

        //call parent start
        base.Start();
    }
	
	// Update is called once per frame
	void Update () {

        //***********************************
        //CARDINAL MOVEMENT [ie: SPYRO]
        //***********************************

        //Move North (up)
        if (Input.GetButton("Up")
        //No other input
        && !Input.GetButton("Down") && !Input.GetButton("Left") && !Input.GetButton("Right"))
        {
            pawn.MoveN();

        }
        //--------------------------------------------------------------------------------------------
        //Move NorthEast (up + right)
	    if (Input.GetButton("Up") && Input.GetButton("Right") 
            //No other input
            && !Input.GetButton("Down") && !Input.GetButton("Left"))

        {
            pawn.MoveNE();
        }
        //----------------------------------------------------------------------------------------------
        //Move East (right)
        if (Input.GetButton("Right") 
            //No other input
            && !Input.GetButton("Up") && !Input.GetButton("Down") && !Input.GetButton("Left"))
        {
            pawn.MoveE();
        }
        //----------------------------------------------------------------------------------------------
        //Move SouthEast (right + down)
        if (Input.GetButton("Right") && Input.GetButton("Down")
            //No other input
            && !Input.GetButton("Up") && !Input.GetButton("Left"))
        {

            pawn.MoveSE();
        }
        //---------------------------------------------------------------------------------------------
        //Move South (down)
        if (Input.GetButton("Down") 
            //No other input
            && !Input.GetButton("Up") && !Input.GetButton("Left") && !Input.GetButton("Right"))
        {
            pawn.MoveS();
        }
        //-------------------------------------------------------------------------------------------------
        //Move SouthWest (left + down)
        if (Input.GetButton("Left") && Input.GetButton("Down")
            //No other input
            && !Input.GetButton("Up") && !Input.GetButton("Right"))
        {
            pawn.MoveSW();
        }

        //Move West (left)
        if (Input.GetButton("Left") 
            //No other input
            && !Input.GetButton("Up") && !Input.GetButton("Down") && !Input.GetButton("Right"))
        {
            pawn.MoveW();
        }

        //Move NorthWest (left + up)
        if (Input.GetButton("Left") && Input.GetButton("Up")
            //No other input
            && !Input.GetButton("Down") && !Input.GetButton("Right"))
        {
            pawn.MoveNW();
        }

        //***********************************
        //TANK MOVEMENT [ie: FLAMES]
        //***********************************

    } //end of update block

} //end of class block
