﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

    //DECLARE VARIABLES
    //-----------------------------------------
    public AI_Hearing ai_hear; //var to store AI hearing
    public AI_Sight ai_see; // var to store AI sight
    private Transform tf; 
    //Finte State Machine
    public enum AIStates
    {
        Patrol, //0
        Investigate, //1
        Chase //2
    }

    public AIStates currentState; //var to hold current state of AI
    public float stopChaseDist; //distance when ai stops chasing

    //START
    //----------------------------------------
    public override void Start () {

        //Run base start
        base.Start();

        //Set variables
        ai_hear = GetComponent<AI_Hearing>();
        ai_see = GetComponent<AI_Sight>();
        tf = GetComponent<Transform>();

	}
	
	// Update is called once per frame
	void Update () {

        // AI States are based on enum value
        switch (currentState)
        {
            case AIStates.Patrol:
                //Call pawn patrol function
                pawn.Patrol();

                // Check for Transitions
                if (ai_hear.CanHear(GameManager.instance.playerController.gameObject)) //if AI hears player pawn
                {
                    //Switch to investigate
                    currentState = AIStates.Investigate;
                }

                if (ai_see.CanSee(GameManager.instance.playerController.gameObject)) //if AI sees player pawn
                {
                    //Switch to chase
                    currentState = AIStates.Chase;
                }
                break;

            case AIStates.Investigate:
                // Call pawn investigate function
                pawn.Investigate();
                // Check for Transitions
                if (ai_see.CanSee(GameManager.instance.playerController.gameObject)) //can see player
                {
                    currentState = AIStates.Chase; //begin chase
                }

                else if (Vector3.Distance(tf.position, GameManager.instance.playerController.gameObject.transform.position) > stopChaseDist) //player is out of range
                {
                    currentState = AIStates.Patrol; //resume patrol
                }
                else if (!ai_hear.CanHear(GameManager.instance.playerController.gameObject)) //can no longer hear player
                {
                    currentState = AIStates.Patrol; //resume patrol
                }
                break;

            case AIStates.Chase:
                // call pawn chase function
                pawn. Chase();
                // Check for Transitions
                if (!ai_see.CanSee(GameManager.instance.playerController.gameObject)) // can no longer see player
                {
                    currentState = AIStates.Investigate; //investigate
                }
                if (Vector3.Distance(tf.position, GameManager.instance.playerController.gameObject.transform.position) > stopChaseDist) //player is out of range
                {
                    currentState = AIStates.Patrol; //resume patrol
                }
                break;


        }

    }
}
