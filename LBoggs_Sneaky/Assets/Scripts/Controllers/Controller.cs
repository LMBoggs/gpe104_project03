﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{

    //Declare variables
    public Pawn pawn; // var to hold pawn for this controller

    // Use this for initialization
    //This will be overriden by child classes
    public virtual void Start()
    {
        //Set pawn to the component on this unique object
        pawn = GetComponent<Pawn>();
    }
}
