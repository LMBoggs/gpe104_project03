﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //DECLARE VARIABLES
    //----------------------------------
    //Management Variables
    public static GameManager instance; //singleton var for game manager
    public PlayerController playerController; // variable for player controller
    public enum GameState // List of states the game itself can be in, cast to an integer(?)
    {
        Title, //0
        GamePlay, //1
        Victory, //2 
        GameOver //3
    }
    public GameState gameState; //var to hold current state of game

    //UI and Screen Variables
    public GameObject TitleScreen;
    public GameObject GameplayScreen;
    public GameObject VictoryScreen;
    public GameObject GameOverScreen;

    //*SPECIAL* Gameplay objects
    public GameObject levelObjects; //An empty parent object containing all level objects
                                       //I did this as a work around to changing scenes. 

    //Gameplay variables
    public float MAX_TIME; //max time to complete level, designer-friendly
    public float timeRemaining; //time remaining in the level
    public int gemsCollected; // var to hold gems currently collected
    public int gemsTotal; // how many gems exist in the game
    public int playerLives; // how many lives the player has, designer-friendly
    public int START_LIVES; // lives the player starts out with, designer friendly

    public List<GameObject> GemsList; // list to hold all gem objects


    //AWAKE
    //----------------------------------------
    void Awake()
    {
        //Initialize this Game Manager
        // As long as there is not an instance already set
        if (instance == null)
        {
            instance = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one - this new object must die
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }    
		
	} //end of Awake()

    //START
    //-----------------------------------------------
    private void Start()
    {
        SetGameState((int)GameState.Title); //start game on title screen
    }

    //UPDATE
    //----------------------------------------------------
    void Update()
    {

        
    }//end of Update()

    void ActivateScreens()
    {
        switch (gameState)
        {
            //Load Title Screen
            case GameState.Title:
                //Set screens
                TitleScreen.SetActive(true);
                GameplayScreen.SetActive(false);
                GameOverScreen.SetActive(false);
                VictoryScreen.SetActive(false);

                //Set objects
                levelObjects.SetActive(false); //*special* turn off level objects not held in gameplay canvas

                break;

            //Run the Game
            case GameState.GamePlay:
                //Set screens
                TitleScreen.SetActive(false);
                GameplayScreen.SetActive(true);
                GameOverScreen.SetActive(false);
                VictoryScreen.SetActive(false);

                //Set objects
                levelObjects.SetActive(true); //*special* turn ON level objects not held in gameplay canvas

                //ReSet gameplay variables on load
                timeRemaining = MAX_TIME; //Set timer to full time
                gemsCollected = 0; // reset number of gems collected

                break;

            //Load Victory Screen
            case GameState.Victory:
                //Set screens
                TitleScreen.SetActive(false);
                GameplayScreen.SetActive(false);
                GameOverScreen.SetActive(false);
                VictoryScreen.SetActive(true);

                //Set objects
                levelObjects.SetActive(false); //*special* turn off level objects not held in gameplay canvas

                break;
            
            //Load Game Over Screen
            case GameState.GameOver:
                //Set screens
                TitleScreen.SetActive(false);
                GameplayScreen.SetActive(false);
                GameOverScreen.SetActive(true);
                VictoryScreen.SetActive(false);

                //Set objects
                levelObjects.SetActive(false); //*special* turn off level objects not held in gameplay canvas
                
                break;

        }//end of switch GameState

    }//End of Activate Screens

    //Set state based on int from Game State enum
    public void SetGameState(int state)
    {
        gameState = (GameState)state;
        ActivateScreens();
    }

    //Quit the game
    public void QuitGame()
    {
        Application.Quit();
    }

}//END OF CLASS



