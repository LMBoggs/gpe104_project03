﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CollectGem : MonoBehaviour {

    //Declare Variables
    public TextMeshProUGUI GemsHUD; //public textpro text to update
    private string gemsCollectedString; //gems cast to string
    private string gemsTotalString;

    public AudioSource gemSound; 


    // When gem is enabled
    void Start () {

        //Add this gem to count in world
        GameManager.instance.gemsTotal += 1;

        //Add this item to the Gems array
        GameManager.instance.GemsList.Add(this.gameObject);

        //store gems collected int as cast to string
        gemsCollectedString = GameManager.instance.gemsCollected.ToString();

        //store gems total int as cast to string
        gemsTotalString = GameManager.instance.gemsTotal.ToString();

        //display gems collected as string in HUD
        GemsHUD.SetText(gemsCollectedString + " / " + gemsTotalString);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        //If the player collided with this object
        if (other.gameObject.tag == "Player")
        {
            //Play Sound
            gemSound.Play();

            //Add 1 to gems collected
            GameManager.instance.gemsCollected += 1;

            //store gems collected int as cast to string
            gemsCollectedString = GameManager.instance.gemsCollected.ToString();

            //store gems total int as cast to string
            gemsTotalString = GameManager.instance.gemsTotal.ToString();

            //display gems collected as string in HUD
            GemsHUD.SetText(gemsCollectedString + " / " + gemsTotalString);

            //set this object to inactive
            gameObject.SetActive(false);
        }
        
    }
}
