﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Gameplay : MonoBehaviour {


    public TextMeshProUGUI Lives; // textMeshPro object to display lives remaining
                                  //Declare Variables
    public TextMeshProUGUI GemsHUD; //public textpro text to update
    private string gemsCollectedString; //gems cast to string
    private string gemsTotalString;

    public PlayerController Player; //store player controller
    public AIController AI; // store ai controller



    // Use this for initialization
    void Start()
    {
        //Set time to max from game manager when level starts
        GameManager.instance.timeRemaining = GameManager.instance.MAX_TIME;

        //Set player lives to starting number
        GameManager.instance.playerLives = GameManager.instance.START_LIVES;
    }
    private void OnEnable()
    {
        //Set time to max from game manager when level is enabled (for good measure)
        GameManager.instance.timeRemaining = GameManager.instance.MAX_TIME;

        //Set gemsCollected to 0
        GameManager.instance.gemsCollected = 0;

        //store gems collected int as cast to string
        gemsCollectedString = GameManager.instance.gemsCollected.ToString();

        //store gems total int as cast to string
        gemsTotalString = GameManager.instance.gemsTotal.ToString();

        //display gems collected as string in HUD
        GemsHUD.SetText(gemsCollectedString + " / " + gemsTotalString);

        //Set player lives to starting number
        GameManager.instance.playerLives = GameManager.instance.START_LIVES;

        //Activate any previously disabled gems
        foreach (GameObject gem in GameManager.instance.GemsList)
        {
            gem.SetActive(true);
        }

        //Set Player and AI controllers to start positions
        Player.pawn.TeleportStart();
        AI.pawn.TeleportStart();

        //Reset Diamond's waypoint management
        AI.GetComponent<DiamondPawn>().wp_index = 0; //

    }

    // Update is called once per frame
    void Update()
    {
        //update each frame (don't do this)
        SetLivesHUD();
        
        //When time reaches 0
        if (GameManager.instance.timeRemaining <= 0) 
        {
            //if the player doesn't have all the gems
            if (GameManager.instance.gemsCollected < GameManager.instance.gemsTotal)
            {
                //Switch to game over
                GameManager.instance.SetGameState((int)GameManager.GameState.GameOver);
            }

            //the player won at the last moment 
            else
            {
                //load victory screen
                GameManager.instance.SetGameState((int)GameManager.GameState.Victory);
            }
            
        }

        //When player collects all the gems
        if (GameManager.instance.gemsCollected >= GameManager.instance.gemsTotal)
        {
            //Load victory screen
            GameManager.instance.SetGameState((int)GameManager.GameState.Victory);
        }

        //If player loses all lives
        if (GameManager.instance.playerLives <= 0)
        {
            //Load GameOver Screen
            GameManager.instance.SetGameState((int)GameManager.GameState.GameOver);
        }

        //For testing
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.instance.SetGameState((int)GameManager.GameState.Victory);
        }
    } //end of Update

    void SetLivesHUD()
    {
        //Store player lives int as string
        string livesString = GameManager.instance.playerLives.ToString();

        //Update HUD lives text with that string
        Lives.SetText(livesString);
    }

} //end of class
