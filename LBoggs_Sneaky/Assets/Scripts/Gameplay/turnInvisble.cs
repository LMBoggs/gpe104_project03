﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turnInvisble : MonoBehaviour {

    private SpriteRenderer mySprite; //var to hold my sprite

	// Use this for initialization
	void Start () {

        //load sprite component
        mySprite = GetComponent<SpriteRenderer>();

        //Disable sprite on start
        mySprite.enabled = false; 

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
