﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelTimer : MonoBehaviour {

    //Declare variables
    private Image timerSprite; //variable to hold our timer sprite
    private float timerPercent; // variable to hold percentage of timer to draw
    public Color firstEighthColor; 
    public Color secondEighthColor; 
    public Color thirdEighthColor; 
    public Color fourthEighthColor;
    public Color fifthEighthColor;
    public Color sixthEighthColor;
    public Color seventhEighthColor;
    public Color eighthEighthColor;

   

    // Use this for initialization
    void Start () {

        //Set variables
        timerSprite = GetComponent<Image>(); //get the image from this object

        // Make sure the image component is set to filled
        timerSprite.type = Image.Type.Filled;


    }
	
	// Update is called once per frame
	void Update () {

        GameManager.instance.timeRemaining -= 1 * Time.deltaTime; //decrement time remaining

        //Update sprite image fill
        //----------------------------
        timerPercent = GameManager.instance.timeRemaining / GameManager.instance.MAX_TIME; //get current percentage of time

        timerSprite.fillAmount = timerPercent; //set sprite draw to time percent

        //Update sprite color
        //----------------------------
        // 8 of 8
        if (timerPercent >= 0.875)
        {
            timerSprite.color = firstEighthColor;
        }

        // 7 of 8
        else if (timerPercent >= 0.75 && timerPercent < 0.875 )
        {
            timerSprite.color = secondEighthColor;
        }

        // 6 of 8
        else if (timerPercent >= 0.625 && timerPercent < 0.75)
        {
            timerSprite.color = thirdEighthColor;
        }

        // 5 of 8
        else if (timerPercent >= 0.5 && timerPercent < 0.625)
        {
            timerSprite.color = fourthEighthColor;
        }

        // 4 of 8
        else if (timerPercent >= 0.375 && timerPercent < 0.5)
        {
            timerSprite.color = fifthEighthColor;
        }

        // 3 of 8
        else if (timerPercent >= 0.25 && timerPercent < 0.375)
        {
            timerSprite.color = sixthEighthColor;
        }

        // 2 of 8
        else if (timerPercent >= 0.125 && timerPercent < 0.25)
        {
            timerSprite.color = seventhEighthColor;
        }

        // 1 of 8
        else if (timerPercent >= 0 && timerPercent < 0.125)
        {
            timerSprite.color = eighthEighthColor;
        }

    }
}
