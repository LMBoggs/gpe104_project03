﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour {

    //Declare varibles
    public float volume; //simple variable to store how loud this object is (for use with ai that can 'hear')
    public float volumeDecay; // rate volume decays per frame, as float. Designer friendly

    void Update()
    {
        //If volume still exists (above 0)
        if (volume > 0)
        {
            //Decrease volume on every frame
            volume = volume - volumeDecay;
        }

        //Fail Safe, no neg. volume
        if (volume < 0)
        {
            volume = 0;
        }

    }
}

