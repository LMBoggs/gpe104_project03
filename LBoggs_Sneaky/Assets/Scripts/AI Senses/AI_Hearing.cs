﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Hearing : MonoBehaviour {

    //Declare variables
    public float hearingThresh; // designer-friendly var to hold the AI's quality of hearing (threshold)
    private NoiseMaker tNoiseMaker; //var to hold noisemaker of target
    private Transform tf; //my transform

    public bool CanHear(GameObject target)
    {
        // Get the target's NoiseMaker
        tNoiseMaker = target.GetComponent<NoiseMaker>();

        // If they don't have one, they can't make noise, so return false
        if (tNoiseMaker == null)
        {
            return false;
        }

        // However, If they do have one
        Transform targetTf = target.GetComponent<Transform>(); //get target tf
        tf = GetComponent<Transform>(); //get my tf
        //If the distance and volume are within my hearing threshhold
        if (Vector3.Distance(targetTf.position, tf.position) <= tNoiseMaker.volume * hearingThresh)
        {
            return true;
        }
        
        // Otherwise
        else
        {
            // we did not hear it, return false
            return false;
        }
        
        
    }


}

