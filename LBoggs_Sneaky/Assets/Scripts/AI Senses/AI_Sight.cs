﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Sight : MonoBehaviour
{

    //Declare variables
    public float fieldOfView = 180.0f; // variable to hold our field of view
    private Vector3 targetPosition; // variable to store the Target's position
    private Vector3 myPosition; //variable to store my position
    private Vector3 lineToTarget; //vector between my position and target position
    private float angleToTarget; //float to store our angle to target
    public float viewDistance = 10; // designer friendly float for view distance

    public Transform tf;

    private void Start()
    {
        tf = GetComponent<Transform>();
    }


     public bool CanSee(GameObject target)
     {

         // Find the target's position
         targetPosition = target.GetComponent<Transform>().position;

         // Store my current position
         myPosition = GetComponent<Transform>().position;

         // Find the vector from the agent to the target
         lineToTarget = targetPosition - myPosition;
         lineToTarget.Normalize(); 

         // Find the angle between the direction our agent is facing (forward in local space) and the vector to the target.
         angleToTarget = Vector3.Angle(lineToTarget, transform.right);



         // if that angle is less than our field of view (target is in vision cone)
         if (angleToTarget < fieldOfView)
         {

             //Conduct Raycast between me and my target
             RaycastHit2D hitInfo = Physics2D.Raycast(myPosition, lineToTarget);

            //If I successfully see my target (no visual obstuctions)
             if (hitInfo.collider.gameObject == target)
             {
                 return true;
             }


         }

         //target is not in cone or is obstructed
         return false;

     }// end of CanSee()

   

} //end of class


